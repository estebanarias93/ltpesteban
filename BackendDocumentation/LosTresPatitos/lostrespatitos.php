<?php

use Sami\Sami;
use Sami\RemoteRepository\BitBucketRemoteRepository;
use Sami\Version\GitVersionCollection;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->in($dir = 'C:\xampp\htdocs\TresPatitos\Proyecto\src')
;

// generate documentation for all v2.0.* tags, the 2.0 branch, and the master one
$versions = GitVersionCollection::create($dir)
    ->add('master', 'master branch')
;

return new Sami($iterator, array(
    'versions'             => $versions,
    'title'                => 'LosTresPatitos Symfony2.7 API',
    'build_dir'            => __DIR__.'/../build/sf2/%version%',
    'cache_dir'            => __DIR__.'/../cache/sf2/%version%',
    'remote_repository'    => new BitBucketRemoteRepository('estebanarias93/ltpesteban', dirname($dir)),
    'default_opened_level' => 2,
));
