// Init loading
setTimeout(function() {
    wdtLoading.start();
}, 20);
var tableContainer = {
    'init' : // Initialize the caseIndex
    function() {
        // Reset document binds
        // Initialiee the jquery related modals
        tableContainer.initJqueryModals();
        // Initialize custom functionality for the modal footers
        tableContainer.bindModalActions();
        // Change datatable error logging to throw instead of alert
        $.fn.dataTableExt.sErrMode = 'throw';
    },
    'bindModalActions' : // Bind special events on the modal footers
    function() {
        // Delete action
        $(document).on("click", ".deleteElement", function(){
            $(this).parent().parent().find(".delete-form > form").eq(0).submit();
        });
        // Save action
        $(document).on("click", ".saveElement", function(){
            $(this).parent().parent().find(".modal-body > form").eq(0).submit();
        });
    },
    'initJqueryModals': // Bind datatable modals
    function() {
        $(document).on("click", ".jquery-modal", function(event){
            $.isLoading({text:"Loading..."});
            event.preventDefault();
            var elementAction = $(this).attr("href");
            $.ajax({
                url: elementAction,
                method: 'GET',
                dataType: 'html',
                success: function(res) {
                    // Open the modal
                    $.isLoading("hide");
                    $("#jqueryModal .modal-content").html(res);
                    $("#jqueryModal").modal();
                    $("body.modal-open").removeAttr("style");
                }
            });
        });
        tableContainer.initJqueryModalFormSubmission();
    },
    'initJqueryModalFormSubmission': // Bind the form submissions that take place within the JQuery Modal
    function(){
        $(document).on("submit", "#jqueryModal form", function(event){
            event.preventDefault();
            var form = event.target;
            $.isLoading({text:"Loading..."});
            $.ajax({
                url: $(this).attr("action"),
                method: 'POST',
                data: new FormData(form),
                processData: false,
                contentType: false,
                success: function(res) {
                    $.isLoading("hide");
                    var hasErrors = false;

                    for(var error in res.errors){
                        hasErrors = true;
                        //Do stuff with errors
                        var error = noty({
                            text: res.errors[error],
                            timeout: 8000,
                            layout: 'center',
                            type: 'error',
                            animation: {
                                open: 'animated zoomIn', // jQuery animate function property object
                                close: 'animated zoomOut', // jQuery animate function property object
                                easing: 'swing', // easing
                                speed: 350 // opening & closing animation speed
                            }
                        });
                    }

                    if(!hasErrors){
                        $(".modal-header > button").click();
                        $("div.dataTables_wrapper table").dataTable().fnPageChange("first",1);
                        var result = noty({
                            text: res.message,
                            layout: 'center',
                            timeout: 5000,
                            type: 'success',
                            animation: {
                                open: 'animated zoomIn', // jQuery animate function property object
                                close: 'animated zoomOut', // jQuery animate function property object
                                easing: 'swing', // easing
                                speed: 350 // opening & closing animation speed
                            }
                        });
                    }

                }
            });
        });
    }
}
