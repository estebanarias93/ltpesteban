
(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">TresPatitos</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:TresPatitos_ArticulosBundle" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle.html">ArticulosBundle</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:TresPatitos_ArticulosBundle_Admin" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/Admin.html">Admin</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_Admin_ArticleAdmin" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Admin/ArticleAdmin.html">ArticleAdmin</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Admin_StoreAdmin" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Admin/StoreAdmin.html">StoreAdmin</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:TresPatitos_ArticulosBundle_Controller" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/Controller.html">Controller</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_Controller_ArticleController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Controller/ArticleController.html">ArticleController</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Controller_ArticleRestController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Controller/ArticleRestController.html">ArticleRestController</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Controller_DefaultController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Controller/DefaultController.html">DefaultController</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Controller_StoreController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Controller/StoreController.html">StoreController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:TresPatitos_ArticulosBundle_Datatables" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/Datatables.html">Datatables</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_Datatables_ArticleDatatable" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html">ArticleDatatable</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Datatables_StoreDatatable" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html">StoreDatatable</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:TresPatitos_ArticulosBundle_DependencyInjection" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/DependencyInjection.html">DependencyInjection</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_DependencyInjection_Configuration" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/DependencyInjection/Configuration.html">Configuration</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_DependencyInjection_TresPatitosArticulosExtension" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/DependencyInjection/TresPatitosArticulosExtension.html">TresPatitosArticulosExtension</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:TresPatitos_ArticulosBundle_Entity" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/Entity.html">Entity</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_Entity_Article" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Entity/Article.html">Article</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Entity_ArticleRepository" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Entity/ArticleRepository.html">ArticleRepository</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Entity_Store" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Entity/Store.html">Store</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Entity_StoreRepository" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Entity/StoreRepository.html">StoreRepository</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:TresPatitos_ArticulosBundle_Form" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="TresPatitos/ArticulosBundle/Form.html">Form</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:TresPatitos_ArticulosBundle_Form_ArticleType" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Form/ArticleType.html">ArticleType</a>                    </div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_Form_StoreType" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/Form/StoreType.html">StoreType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:TresPatitos_ArticulosBundle_TresPatitosArticulosBundle" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="TresPatitos/ArticulosBundle/TresPatitosArticulosBundle.html">TresPatitosArticulosBundle</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "TresPatitos.html", "name": "TresPatitos", "doc": "Namespace TresPatitos"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle.html", "name": "TresPatitos\\ArticulosBundle", "doc": "Namespace TresPatitos\\ArticulosBundle"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/Admin.html", "name": "TresPatitos\\ArticulosBundle\\Admin", "doc": "Namespace TresPatitos\\ArticulosBundle\\Admin"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/Controller.html", "name": "TresPatitos\\ArticulosBundle\\Controller", "doc": "Namespace TresPatitos\\ArticulosBundle\\Controller"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/Datatables.html", "name": "TresPatitos\\ArticulosBundle\\Datatables", "doc": "Namespace TresPatitos\\ArticulosBundle\\Datatables"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/DependencyInjection.html", "name": "TresPatitos\\ArticulosBundle\\DependencyInjection", "doc": "Namespace TresPatitos\\ArticulosBundle\\DependencyInjection"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/Entity.html", "name": "TresPatitos\\ArticulosBundle\\Entity", "doc": "Namespace TresPatitos\\ArticulosBundle\\Entity"},{"type": "Namespace", "link": "TresPatitos/ArticulosBundle/Form.html", "name": "TresPatitos\\ArticulosBundle\\Form", "doc": "Namespace TresPatitos\\ArticulosBundle\\Form"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Admin", "fromLink": "TresPatitos/ArticulosBundle/Admin.html", "link": "TresPatitos/ArticulosBundle/Admin/ArticleAdmin.html", "name": "TresPatitos\\ArticulosBundle\\Admin\\ArticleAdmin", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Admin", "fromLink": "TresPatitos/ArticulosBundle/Admin.html", "link": "TresPatitos/ArticulosBundle/Admin/StoreAdmin.html", "name": "TresPatitos\\ArticulosBundle\\Admin\\StoreAdmin", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Controller", "fromLink": "TresPatitos/ArticulosBundle/Controller.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "doc": "&quot;Article controller.&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_indexAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::indexAction", "doc": "&quot;Lists all Article entities.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_indexResultsAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::indexResultsAction", "doc": "&quot;Lists all Article entities for datatable.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_createAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::createAction", "doc": "&quot;Creates a new Article entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_newAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::newAction", "doc": "&quot;Displays a form to create a new Article entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_showAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::showAction", "doc": "&quot;Finds and displays a Article entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_editAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::editAction", "doc": "&quot;Displays a form to edit an existing Article entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_updateAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::updateAction", "doc": "&quot;Edits an existing Article entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleController.html#method_deleteAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleController::deleteAction", "doc": "&quot;Deletes a Article entity.&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Controller", "fromLink": "TresPatitos/ArticulosBundle/Controller.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html#method_getArticlesAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController::getArticlesAction", "doc": "&quot;Get all articles.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html#method_getStoresAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController::getStoresAction", "doc": "&quot;Get all stores.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController", "fromLink": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html", "link": "TresPatitos/ArticulosBundle/Controller/ArticleRestController.html#method_getArticlesByStoreAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\ArticleRestController::getArticlesByStoreAction", "doc": "&quot;Get all articles by Store ID.&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Controller", "fromLink": "TresPatitos/ArticulosBundle/Controller.html", "link": "TresPatitos/ArticulosBundle/Controller/DefaultController.html", "name": "TresPatitos\\ArticulosBundle\\Controller\\DefaultController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\DefaultController", "fromLink": "TresPatitos/ArticulosBundle/Controller/DefaultController.html", "link": "TresPatitos/ArticulosBundle/Controller/DefaultController.html#method_indexAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\DefaultController::indexAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Controller", "fromLink": "TresPatitos/ArticulosBundle/Controller.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "doc": "&quot;Store controller.&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_indexAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::indexAction", "doc": "&quot;Lists all Store entities.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_indexResultsAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::indexResultsAction", "doc": "&quot;Lists all Store entities for datatable.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_createAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::createAction", "doc": "&quot;Creates a new Store entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_newAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::newAction", "doc": "&quot;Displays a form to create a new Store entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_showAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::showAction", "doc": "&quot;Finds and displays a Store entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_editAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::editAction", "doc": "&quot;Displays a form to edit an existing Store entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_updateAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::updateAction", "doc": "&quot;Edits an existing Store entity.&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Controller\\StoreController", "fromLink": "TresPatitos/ArticulosBundle/Controller/StoreController.html", "link": "TresPatitos/ArticulosBundle/Controller/StoreController.html#method_deleteAction", "name": "TresPatitos\\ArticulosBundle\\Controller\\StoreController::deleteAction", "doc": "&quot;Deletes a Store entity.&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Datatables", "fromLink": "TresPatitos/ArticulosBundle/Datatables.html", "link": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html", "name": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable", "doc": "&quot;Class ArticleDatatable&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html#method_buildDatatable", "name": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable::buildDatatable", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html#method_getEntity", "name": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable::getEntity", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/ArticleDatatable.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Datatables\\ArticleDatatable::getName", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Datatables", "fromLink": "TresPatitos/ArticulosBundle/Datatables.html", "link": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html", "name": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable", "doc": "&quot;Class StoreDatatable&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html#method_buildDatatable", "name": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable::buildDatatable", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html#method_getEntity", "name": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable::getEntity", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable", "fromLink": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html", "link": "TresPatitos/ArticulosBundle/Datatables/StoreDatatable.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Datatables\\StoreDatatable::getName", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\DependencyInjection", "fromLink": "TresPatitos/ArticulosBundle/DependencyInjection.html", "link": "TresPatitos/ArticulosBundle/DependencyInjection/Configuration.html", "name": "TresPatitos\\ArticulosBundle\\DependencyInjection\\Configuration", "doc": "&quot;This is the class that validates and merges configuration from your app\/config files&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\DependencyInjection\\Configuration", "fromLink": "TresPatitos/ArticulosBundle/DependencyInjection/Configuration.html", "link": "TresPatitos/ArticulosBundle/DependencyInjection/Configuration.html#method_getConfigTreeBuilder", "name": "TresPatitos\\ArticulosBundle\\DependencyInjection\\Configuration::getConfigTreeBuilder", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\DependencyInjection", "fromLink": "TresPatitos/ArticulosBundle/DependencyInjection.html", "link": "TresPatitos/ArticulosBundle/DependencyInjection/TresPatitosArticulosExtension.html", "name": "TresPatitos\\ArticulosBundle\\DependencyInjection\\TresPatitosArticulosExtension", "doc": "&quot;This is the class that loads and manages your bundle configuration&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\DependencyInjection\\TresPatitosArticulosExtension", "fromLink": "TresPatitos/ArticulosBundle/DependencyInjection/TresPatitosArticulosExtension.html", "link": "TresPatitos/ArticulosBundle/DependencyInjection/TresPatitosArticulosExtension.html#method_load", "name": "TresPatitos\\ArticulosBundle\\DependencyInjection\\TresPatitosArticulosExtension::load", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Entity", "fromLink": "TresPatitos/ArticulosBundle/Entity.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article", "doc": "&quot;Article&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getId", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setName", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setName", "doc": "&quot;Set name&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getName", "doc": "&quot;Get name&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setDescription", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setDescription", "doc": "&quot;Set description&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getDescription", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getDescription", "doc": "&quot;Get description&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setPrice", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setPrice", "doc": "&quot;Set price&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getPrice", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getPrice", "doc": "&quot;Get price&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setTotalInShelf", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setTotalInShelf", "doc": "&quot;Set totalInShelf&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getTotalInShelf", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getTotalInShelf", "doc": "&quot;Get totalInShelf&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setTotalInVault", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setTotalInVault", "doc": "&quot;Set totalInVault&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getTotalInVault", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getTotalInVault", "doc": "&quot;Get totalInVault&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_setStore", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::setStore", "doc": "&quot;Set store&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Article", "fromLink": "TresPatitos/ArticulosBundle/Entity/Article.html", "link": "TresPatitos/ArticulosBundle/Entity/Article.html#method_getStore", "name": "TresPatitos\\ArticulosBundle\\Entity\\Article::getStore", "doc": "&quot;Get store&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Entity", "fromLink": "TresPatitos/ArticulosBundle/Entity.html", "link": "TresPatitos/ArticulosBundle/Entity/ArticleRepository.html", "name": "TresPatitos\\ArticulosBundle\\Entity\\ArticleRepository", "doc": "&quot;ArticleRepository&quot;"},
                    
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Entity", "fromLink": "TresPatitos/ArticulosBundle/Entity.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store", "doc": "&quot;Store&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method___construct", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_getId", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_setName", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::setName", "doc": "&quot;Set name&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::getName", "doc": "&quot;Get name&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_setAddress", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::setAddress", "doc": "&quot;Set address&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_getAddress", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::getAddress", "doc": "&quot;Get address&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_addArticle", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::addArticle", "doc": "&quot;Add articles&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_removeArticle", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::removeArticle", "doc": "&quot;Remove articles&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method_getArticles", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::getArticles", "doc": "&quot;Get articles&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Entity\\Store", "fromLink": "TresPatitos/ArticulosBundle/Entity/Store.html", "link": "TresPatitos/ArticulosBundle/Entity/Store.html#method___toString", "name": "TresPatitos\\ArticulosBundle\\Entity\\Store::__toString", "doc": "&quot;Render a Store as a string&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Entity", "fromLink": "TresPatitos/ArticulosBundle/Entity.html", "link": "TresPatitos/ArticulosBundle/Entity/StoreRepository.html", "name": "TresPatitos\\ArticulosBundle\\Entity\\StoreRepository", "doc": "&quot;StoreRepository&quot;"},
                    
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Form", "fromLink": "TresPatitos/ArticulosBundle/Form.html", "link": "TresPatitos/ArticulosBundle/Form/ArticleType.html", "name": "TresPatitos\\ArticulosBundle\\Form\\ArticleType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\ArticleType", "fromLink": "TresPatitos/ArticulosBundle/Form/ArticleType.html", "link": "TresPatitos/ArticulosBundle/Form/ArticleType.html#method_buildForm", "name": "TresPatitos\\ArticulosBundle\\Form\\ArticleType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\ArticleType", "fromLink": "TresPatitos/ArticulosBundle/Form/ArticleType.html", "link": "TresPatitos/ArticulosBundle/Form/ArticleType.html#method_setDefaultOptions", "name": "TresPatitos\\ArticulosBundle\\Form\\ArticleType::setDefaultOptions", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\ArticleType", "fromLink": "TresPatitos/ArticulosBundle/Form/ArticleType.html", "link": "TresPatitos/ArticulosBundle/Form/ArticleType.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Form\\ArticleType::getName", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle\\Form", "fromLink": "TresPatitos/ArticulosBundle/Form.html", "link": "TresPatitos/ArticulosBundle/Form/StoreType.html", "name": "TresPatitos\\ArticulosBundle\\Form\\StoreType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\StoreType", "fromLink": "TresPatitos/ArticulosBundle/Form/StoreType.html", "link": "TresPatitos/ArticulosBundle/Form/StoreType.html#method_buildForm", "name": "TresPatitos\\ArticulosBundle\\Form\\StoreType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\StoreType", "fromLink": "TresPatitos/ArticulosBundle/Form/StoreType.html", "link": "TresPatitos/ArticulosBundle/Form/StoreType.html#method_setDefaultOptions", "name": "TresPatitos\\ArticulosBundle\\Form\\StoreType::setDefaultOptions", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "TresPatitos\\ArticulosBundle\\Form\\StoreType", "fromLink": "TresPatitos/ArticulosBundle/Form/StoreType.html", "link": "TresPatitos/ArticulosBundle/Form/StoreType.html#method_getName", "name": "TresPatitos\\ArticulosBundle\\Form\\StoreType::getName", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "TresPatitos\\ArticulosBundle", "fromLink": "TresPatitos/ArticulosBundle.html", "link": "TresPatitos/ArticulosBundle/TresPatitosArticulosBundle.html", "name": "TresPatitos\\ArticulosBundle\\TresPatitosArticulosBundle", "doc": "&quot;&quot;"},
                    
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


