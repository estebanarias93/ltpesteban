var gulp = require('gulp'),
  gulpif = require('gulp-if'),
  uglify = require('gulp-uglify'),
  uglifycss = require('gulp-uglifycss'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  gutil = require('gulp-util'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  precss = require('precss'),
  colorfunctions = require('postcss-color-function'),
  watch = require('gulp-watch'),
  stripCssComments = require('gulp-strip-css-comments')

  source = 'frontend/',
  dest = 'web/assets/',
  env = process.env.GULP_NEW;

gulp.task('js', function () {
  return gulp.src([
      'frontend/js/*.js'])
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(dest + 'js'));
});

//CSS TASK: write one minified css file out of bootstrap.less and all of my custom less files
gulp.task('css', function () {
    return gulp.src([
        'frontend/css/*.css'])
        .pipe(postcss([
          autoprefixer(),
          colorfunctions(),
          precss()
        ]))
        .pipe(sourcemaps.init())
        .pipe(concat('main.css'))
        .pipe(stripCssComments())
        .pipe(uglifycss({
          "maxLineLen": 80,
          "uglyComments": true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest + 'css'));
});

gulp.task('default', ['js','css']);
