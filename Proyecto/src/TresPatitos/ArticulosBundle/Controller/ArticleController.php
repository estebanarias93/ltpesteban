<?php

namespace TresPatitos\ArticulosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use TresPatitos\ArticulosBundle\Entity\Article;
use TresPatitos\ArticulosBundle\Form\ArticleType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{

    /**
     * Lists all Article entities.
     *
     * @Route("/", name="article")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $datatable = $this->get('app.datatable.article');
        $datatable->buildDatatable();

        return $this->render('TresPatitosArticulosBundle:Article:index.html.twig', array(
            'datatable' => $datatable,
        ));
    }
    /**
     * Lists all Article entities for datatable.
     *
     * @Route("/results", name="article_results")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexResultsAction()
    {
        $datatable = $this->get('app.datatable.article');
        $datatable->buildDatatable();

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        return $query->getResponse();
    }
    /**
     * Creates a new Article entity.
     *
     * @Route("/", name="article_create")
     * @Method("POST")
     * @Template("TresPatitosArticulosBundle:Article:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Article();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $responseJSON = array(
                'status' => 'success',
                'message' => 'The information has been created successfully.'
            );
            $serializer = $this->get('serializer');
            $jsonContent = $serializer->serialize($responseJSON, 'json');
            $response = new Response($jsonContent);
            $response->headers->set('Content-Type', 'application/json');
            return $response;

        } else {
            $message = 'There was a problem while processing the request.';
            $errorReturn = array('There was an error processing the request.');
            if (count($errors)>0){
                foreach ($errors as $error) {
                    $errorReturn[] = "[".$error->getPropertyPath()."] ".$error->getMessage();
                }
            }
            $responseJSON = array(
                'status' => 'error',
                'message' => $message,
                'errors' => $errorReturn
            );
            $serializer = $this->get('serializer');
            $jsonContent = $serializer->serialize($responseJSON, 'json');
            $response = new Response($jsonContent);
            $response->headers->set('Content-Type', 'application/json');
            return $response;

        }
    }

    /**
     * Creates a form to create a Article entity.
     *
     * @param Article $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Article $entity)
    {
        $form = $this->createForm(new ArticleType(), $entity, array(
            'action' => $this->generateUrl('article_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Article entity.
     *
     * @Route("/new", name="article_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Article();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{id}", name="article_show", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TresPatitosArticulosBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TresPatitosArticulosBundle:Article:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{id}/edit", name="article_edit", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TresPatitosArticulosBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Article entity.
    *
    * @param Article $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Article $entity)
    {
        $form = $this->createForm(new ArticleType(), $entity, array(
            'action' => $this->generateUrl('article_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Article entity.
     *
     * @Route("/{id}", name="article_update")
     * @Method("PUT")
     * @Template("TresPatitosArticulosBundle:Article:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TresPatitosArticulosBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors)==0) {
            $em->flush();
            // All went good, lets return the pertaining info
            $response = new Response(json_encode(array(
                'status' => 'success',
                'message' => 'The information has been saved successfully.'
            )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $message = 'There was a problem while processing the request.';
            $errorReturn = array('There was an error processing the request.');
            if (count($errors)>0){
                foreach ($errors as $error) {
                    $errorReturn[] = "[".$error->getPropertyPath()."] ".$error->getMessage();
                }
            }
            $response = new Response(json_encode(array(
                'status' => 'error',
                'message' => $message,
                'errors' => $errorReturn
            )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }
    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TresPatitosArticulosBundle:Store')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Store entity.');
            }

            $em->remove($entity);
            $em->flush();

            $response = new Response(json_encode(array(
                'status' => 'success',
                'message' => 'The information has been deleted successfully.'
            )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;

        } else {
            $response = new Response(json_encode(array(
                'status' => 'error',
                'message' => 'There was an error processing the request.'
            )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * Creates a form to delete a Article entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
