<?php

namespace TresPatitos\ArticulosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        $datatable = $this->get('app.datatable.article');
        $datatable->buildDatatable();

        return $this->render('TresPatitosArticulosBundle:Article:index.html.twig', array(
            'datatable' => $datatable,
        ));
    }
}
