<?php

namespace TresPatitos\ArticulosBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Get;

class ArticleRestController extends FOSRestController
{
    /**
     * Get all articles.
     *
     * @return array
     *
     * @ApiDoc(
     *   resource = true,
     *   https = false,
     *   tags = {
     *      "stable",
     *      "live",
     *      "no-auth",
     *   },
     *   description = "Load all the articles that are in the Database.",
     *   parameters = {
     *      {"name"="username", "dataType"="string", "required"=true, "description"="The requester username for session validation. Sent via GET."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="The requester password for session validation. Sent via GET."}
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      401 = "Returned when Not Authorized",
     *      404 = "Returned when Not Found",
     *      500 = "Returned when there is an Internal Server Error"
     *
     *   }
     * )
     * @Get("/services/articles")
     */
    public function getArticlesAction()
    {
        // Define if Authentication is valid
        $authSuccess = false;
        if (isset($_GET['username'])
            && isset($_GET['password'])
        ){
            // The Request comes with Username and Password...so let's validate them
            if ($_GET['username']=='my_user'
                && $_GET['password']=='my_password'){
                // Authentication successful!
                $authSuccess = true;
            } else {
            // Authentication unsuccessful!
                $authSuccess = false;
            }
        } else {
            // Authentication unsuccessful!
            $authSuccess = false;
        }
        if ($authSuccess){
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('TresPatitosArticulosBundle:Article')->findAll();
            $processedData = [
                "articles" => array(),
                "success" => true,
                "total_elements" => 0
            ];
            foreach ($entities as $article) {
                $articleData = [
                    'id' => $article->getId(),
                    'description' => $article->getDescription(),
                    'name' => $article->getName(),
                    'price' => $article->getPrice(),
                    'total_in_shelf' => $article->getTotalInShelf(),
                    'total_in_valut' => $article->getTotalInVault(),
                    'store_name' => $article->getStore()->getName()
                ];
                $processedData['articles'][] = $articleData;
                $processedData['total_elements']++;
            }
        } else {
            $processedData = [
                "articles" => array(),
                "success" => false,
                "total_elements" => 0
            ];
            $processedData['error_code'] = 401;
            $processedData['error_msg'] = "You are not authorized to view this content.";
            $statusCode = 404;
        }
        $view = View::create()
          ->setStatusCode(200)
          ->setData($processedData)
          ->setFormat('json');
        return $this->get('fos_rest.view_handler')->handle($view);

    }
    /**
     * Get all stores.
     *
     * @return array
     *
     * @ApiDoc(
     *   resource = true,
     *   https = false,
     *   tags = {
     *      "stable",
     *      "live",
     *      "no-auth",
     *   },
     *   description = "Load all the stores that are stored in the Database.",
     *   parameters = {
     *      {"name"="username", "dataType"="string", "required"=true, "description"="The requester username for session validation. Sent via GET."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="The requester password for session validation. Sent via GET."}
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      401 = "Returned when Not Authorized",
     *      404 = "Returned when Not Found",
     *      500 = "Returned when there is an Internal Server Error"
     *
     *   }
     * )
     * @Get("/services/stores")
     */
    public function getStoresAction()
    {
        // Define if Authentication is valid
        $authSuccess = false;
        if (isset($_GET['username'])
            && isset($_GET['password'])
        ){
            // The Request comes with Username and Password...so let's validate them
            if ($_GET['username']=='my_user'
                && $_GET['password']=='my_password'){
                // Authentication successful!
                $authSuccess = true;
            } else {
            // Authentication unsuccessful!
                $authSuccess = false;
            }
        } else {
            // Authentication unsuccessful!
            $authSuccess = false;
        }
        if ($authSuccess){
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('TresPatitosArticulosBundle:Store')->findAll();
            $processedData = [
                "stores" => array(),
                "success" => true,
                "total_elements" => 0
            ];
            foreach ($entities as $store) {
                $storeData = [
                    'id' => $store->getId(),
                    'address' => $store->getAddress(),
                    'name' => $store->getName()
                ];
                $processedData['stores'][] = $storeData;
                $processedData['total_elements']++;
            }
        } else {
           $processedData = [
               "stores" => array(),
               "success" => false,
               "total_elements" => 0
           ];
           $processedData['error_code'] = 401;
           $processedData['error_msg'] = "You are not authorized to view this content.";
           $statusCode = 404;
        }
        $view = View::create()
          ->setStatusCode(200)
          ->setData($processedData)
          ->setFormat('json');
        return $this->get('fos_rest.view_handler')->handle($view);
    }
    /**
     * Get all articles by Store ID.
     *
     * @return array
     *
     * @ApiDoc(
     *   resource = true,
     *   https = false,
     *   tags = {
     *      "stable",
     *      "live",
     *      "no-auth",
     *   },
     *   description = "Get all articles by Store ID.",
     *   parameters = {
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="The Store ID is a required field. Sent via URL Param."},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="The requester username for session validation. Sent via GET."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="The requester password for session validation. Sent via GET."}
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      401 = "Returned when Not Authorized",
     *      404 = "Returned when Not Found",
     *      500 = "Returned when there is an Internal Server Error"
     *
     *   }
     * )
     * @Get("/services/articles/stores/{id}")
     */
    public function getArticlesByStoreAction($id)
    {
        // Define if Authentication is valid
        $authSuccess = false;
        if (isset($_GET['username'])
            && isset($_GET['password'])
        ){
            // The Request comes with Username and Password...so let's validate them
            if ($_GET['username']=='my_user'
                && $_GET['password']=='my_password'){
                // Authentication successful!
                $authSuccess = true;
            } else {
            // Authentication unsuccessful!
                $authSuccess = false;
            }
        } else {
            // Authentication unsuccessful!
            $authSuccess = false;
        }
        if ($authSuccess){
            if (ctype_digit($id)){
                // Int ID
                $em = $this->getDoctrine()->getManager();
                $entities = $em->getRepository('TresPatitosArticulosBundle:Article')->findBy(array('store' => $id ));
                $statusCode = 200;
                $processedData = [
                    "articles" => array(),
                    "success" => true,
                    "total_elements" => 0
                ];
                foreach ($entities as $article) {
                    $articleData = [
                        'id' => $article->getId(),
                        'description' => $article->getDescription(),
                        'name' => $article->getName(),
                        'price' => $article->getPrice(),
                        'total_in_shelf' => $article->getTotalInShelf(),
                        'total_in_valut' => $article->getTotalInVault(),
                        'store_name' => $article->getStore()->getName()
                    ];
                    $processedData['articles'][] = $articleData;
                    $processedData['total_elements']++;
                }
                if (count($processedData['articles'])==0){
                    $processedData['success'] = false;
                    $processedData['error_code'] = 404;
                    $processedData['error_msg'] = "Record not Found";
                    $statusCode = 404;
                }
            } else {
                // Non Int ID
                $processedData['success'] = false;
                $processedData['error_code'] = 400;
                $processedData['error_msg'] = "Bad Request";
                $statusCode = 400;
            }
        } else {
           $processedData = [
               "articles" => array(),
               "success" => false,
               "total_elements" => 0
           ];
           $processedData['error_code'] = 401;
           $processedData['error_msg'] = "You are not authorized to view this content.";
           $statusCode = 404;
        }

        $view = View::create()
          ->setStatusCode($statusCode)
          ->setData($processedData)
          ->setFormat('json');
        return $this->get('fos_rest.view_handler')->handle($view);
    }
}
