<?php

namespace TresPatitos\ArticulosBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StoreControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/trespatitos/articulos/store/list');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /store/");
        $link = $crawler
            ->filter('a:contains("Add new")') // find all links with the text "Greet"
            ->eq(0) // select the first link in the list
            ->link()
        ;
        $crawler = $client->click($link);

        $form = $crawler->selectButton('Create')->form(array(
            'name'  => 'Test',
            'address'  => 'Test address for Unit Testing purposes!',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('[value="Test"]')->count(), 'Missing element [value="Test"]');
        $this->assertGreaterThan(0, $crawler->filter('[value="Test address for Unit Testing purposes!"]')->count(), 'Missing element [value="Test address for Unit Testing purposes!"]');

        $form = $crawler->selectButton('Update')->form(array(
            'trespatitos_articulosbundle_store[name]'  => 'Foo',
            'trespatitos_articulosbundle_store[address]'  => 'Edited the address..for testing purposes!',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('[value="Foo"]')->count(), 'Missing element [value="Foo"]');
        $this->assertGreaterThan(0, $crawler->filter('[value="Edited the address..for testing purposes!"]')->count(), 'Missing element [value="Edited the address..for testing purposes!"]');

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }


}
