<?php

namespace TresPatitos\ArticulosBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ArticleAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')
        ->add("description")
        ->add("price")
        ->add("totalInShelf")
        ->add("totalInVault")
        ->add("store");
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
        ->add('description')
        ->add('price')
        ->add('totalInShelf')
        ->add('totalInVault')
        ->add('store');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
        ->addIdentifier('price')
        ->addIdentifier('totalInShelf')
        ->addIdentifier('totalInVault')
        ->addIdentifier('store');
    }
}
