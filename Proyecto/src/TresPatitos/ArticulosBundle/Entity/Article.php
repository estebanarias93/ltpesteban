<?php

namespace TresPatitos\ArticulosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\Table(name="articles")
 * @ORM\Entity(repositoryClass="TresPatitos\ArticulosBundle\Entity\ArticleRepository")
 */
class Article
{

    /**
     * @var Store
     *
     * @ORM\ManyToOne(targetEntity="Store", inversedBy="articles")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $store;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="total_in_shelf", type="integer")
     */
    private $totalInShelf;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="total_in_vault", type="integer")
     */
    private $totalInVault;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Article
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set totalInShelf
     *
     * @param integer $totalInShelf
     * @return Article
     */
    public function setTotalInShelf($totalInShelf)
    {
        $this->totalInShelf = $totalInShelf;

        return $this;
    }

    /**
     * Get totalInShelf
     *
     * @return integer
     */
    public function getTotalInShelf()
    {
        return $this->totalInShelf;
    }

    /**
     * Set totalInVault
     *
     * @param integer $totalInVault
     * @return Article
     */
    public function setTotalInVault($totalInVault)
    {
        $this->totalInVault = $totalInVault;

        return $this;
    }

    /**
     * Get totalInVault
     *
     * @return integer
     */
    public function getTotalInVault()
    {
        return $this->totalInVault;
    }

    /**
     * Set store
     *
     * @param \TresPatitos\ArticulosBundle\Entity\Store $store
     * @return Store
     */
    public function setStore(\TresPatitos\ArticulosBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \TresPatitos\ArticulosBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
